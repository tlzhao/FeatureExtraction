import numpy as np
import Classifiers as cl
from PCA import PCA
from LDA import LDA
from sklearn.datasets import load_iris
from dataset.LoadSat import LoadSat

"""
data_set = load_iris()
data = np.array(data_set.data)
label = np.array(data_set.target)

train_idx = range(0, 40) + range(50,90) + range(100,140)
test_idx = range(40,50) + range(90,100) + range(140,150)

train_data = data[train_idx,:]
train_label = label[train_idx]

test_data = data[test_idx,:]
test_label = label[test_idx]

pca_train_data, pca_eig_vec = PCA(data, 2)
pca_train_data = train_data.dot(pca_eig_vec)
pca_test_data = test_data.dot(pca_eig_vec)

lda_train_data, lda_eig_vec = LDA(train_data, train_label, 3)
lda_test_data = test_data.dot(lda_eig_vec)

# QDF
print ("Test accuracy of PCA-QDF:")
pca_sigma, pca_mean = cl.QDF(pca_train_data, train_label, 3)
print(cl.QDFTest(pca_test_data, test_label, pca_sigma, pca_mean))

print ("Test accuracy of LDA-QDF")
lda_sigma, lda_mean = cl.QDF(lda_train_data, train_label, 3)
print(cl.QDFTest(lda_test_data, test_label, lda_sigma, lda_mean))

# LDF
print ("Test accuracy of PCA-LDF")
pca_w, pca_b = cl.LDF(pca_train_data, train_label, 3)
print(cl.LDFTest(pca_test_data, test_label, pca_w, pca_b))

print ("Test accuracy of LDA-LDF")
lda_w, lda_b = cl.LDF(lda_train_data, train_label, 3)
print(cl.LDFTest(lda_test_data, test_label, lda_w, lda_b))

# NN
print ("Test accuracy of PCA-1NN")
print(cl.NNTest(pca_test_data, test_label, pca_train_data, train_label))
print ("Test accuracy of LDA-1NN")
print(cl.NNTest(lda_test_data, test_label, lda_train_data, train_label))
"""

def Test(train_data, train_label, test_data, test_label, clusters, n):
    data = np.concatenate((train_data, test_data), axis = 0)
    pca_data, pca_eig_vec = PCA(data, n)
    pca_train_data = np.array(train_data).dot(pca_eig_vec)
    pca_test_data = np.array(test_data).dot(pca_eig_vec)
    
    lda_train_data, lda_eig_vec = LDA(train_data, train_label, clusters)
    lda_test_data = np.array(test_data).dot(lda_eig_vec)

    #QDF
    print ("Test accuracy of PCA-QDF")
    pca_sigma, pca_mean = cl.QDF(pca_train_data, train_label, clusters)
    print(1-cl.QDFTest(pca_test_data, test_label, pca_sigma, pca_mean))

    print ("Test accuracy of LDA-QDF")
    lda_sigma, lda_mean = cl.QDF(lda_train_data, train_label, clusters)
    print(1-cl.QDFTest(lda_test_data, test_label, lda_sigma, lda_mean))

    #LDF
    print ("Test accuracy of PCA-LDF")
    pca_w, pca_b = cl.LDF(pca_train_data, train_label, clusters)
    print (1-cl.LDFTest(pca_test_data, test_label, pca_w, pca_b))

    print ("Test accuracy of LDA-LDF")
    lda_w, lda_b = cl.LDF(lda_train_data, train_label, clusters)
    print (1-cl.LDFTest(lda_test_data, test_label, lda_w, lda_b))

    #NN
    print ("Test accuracy of PCA-1NN")
    print (1-cl.NNTest(pca_test_data, test_label, pca_train_data, train_label))
    print ("Test accuracy of LDA-1NN")
    print (1-cl.NNTest(lda_test_data, test_label, lda_train_data, train_label))


print ('SatImage:')
train_data, train_label, test_data, test_label = LoadSat()
Test(train_data, train_label, test_data, test_label, 6, 5)

print ('Optdigits:')
from dataset.LoadOptDigits import LoadOptDigits
train_data, train_label, test_data, test_label = LoadOptDigits()
Test(train_data, train_label, test_data, test_label, 10, 9)



print ('Vowels:')
from dataset.LoadVowels import LoadVowels
train_data, train_label, test_data, test_label = LoadVowels()
Test(train_data, train_label, test_data, test_label, 9, 8)
