from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

def QDFScore(data, sigma, mean):
    return (data-mean).dot(np.linalg.pinv(sigma)).dot((data-mean).T) + np.log(np.linalg.det(sigma) + 0.01)

def LDFScore(data, w, b):
    return w.dot(data.T) - b

def QDF(data, label, num_class):
    sigma = []
    mean = []
    for cl in range(0,num_class):
        class_data = data[label == cl,:]
        mean.append(np.mean(class_data, axis = 0))
        sigma.append(np.cov(class_data, rowvar = 0))
    return sigma,mean

def LDF(data, label,num_class):
    w = []
    b = []
    sigma = np.cov(data, rowvar = 0)
    for cl in range(0, num_class):
        class_data = data[label == cl,:]
        mean_vec = np.mean(class_data, axis = 0)
        w.append(mean_vec.dot(sigma))
        b.append(mean_vec.dot(sigma).dot(mean_vec.T) / 2)
    return w,b

def KMeans(data, clusters):
    mean = data.mean(axis = 0)
    proto = np.zeros((clusters, data.shape[1]))
    for i in range(0, clusters):
        proto[i,:] = mean * (i+1)/(clusters+1) * 2

    for iters in range(0, 100):
        # Assign each data to its nearest prototype
        idx = []
        for n in range(0, data.shape[0]):
            dis = []
            for p in range(0, clusters):
                e = data[n,:] - proto[p,:]
                dis.append(e.dot(e))
            idx.append(np.argmin(dis))
        idx = np.array(idx)
        # Update prototypes
        for p in range(0, clusters):
            if (data[idx == p,:].size != 0):
                proto[p,:] = data[idx == p, :].mean(axis = 0)

    return proto

def ProtoType(data, label, num_class, num_proto):
    proto_type = []
    proto_label = []

    for c in range(0, num_class):
        class_data = data[label == c, :]
        if c == 0:
            proto_type = KMeans(class_data, num_proto)
        else:
            proto_type = np.concatenate((proto_type, KMeans(class_data, num_proto)), axis = 0)

    for c in range(0, num_class):
        for p in range(num_proto):
            proto_label.append(c)
    proto_label = np.array(proto_label)

    return proto_type, proto_label


def NNClass(data, label, x):
    dis = np.array(data - x)
    d = []
    for i in range(0, len(label)):
        d.append(dis[i,:].dot(dis[i,:].T))
    return label[np.argmin(d)]

def ProtoTypeClass(data, proto, label):
    return NNClass(proto, label, data)

def NNTest(data, label, tdata, tlabel):
    err = 0
    for d in range(0, len(label)):
        lab = NNClass(tdata, tlabel, data[d,:])
        if lab != label[d]:
#            print "Error at index: %d %d(%d)"%(d, lab, label[d])
            err = err + 1
    return err / len(label)

def ProtoTypeTest(data, label, proto, tlabel):
    return NNTest(data, label, proto, tlabel)

def LDFClass(data, w, b):
    s = []
    for weight, bias in zip(w, b):
        s.append(LDFScore(data, weight, bias))
    return np.argmax(s)

def LDFTest(data, label, w, b):
    err = 0
    for d in range(0,len(label)):
        class_data = data[d,:]
        cl = label[d]
        lab = LDFClass(class_data, w, b)
        if lab != cl:
            err = err + 1;
#            print "Error at index: %d %d(%d)"%(d, lab, cl)
    return err / len(label)



def QDFClass(data, sigma, mean):
    s = []
    for i in range(0, len(mean)):
        s.append(QDFScore(data, sigma[i], mean[i]))
    return np.argmin(s)

def QDFTest(data, label, sigma, mean):
    err = 0
    for d in range(0,len(label)):
        class_data = data[d,:]
        cl = label[d]
        lab = QDFClass(class_data, sigma, mean)
        if lab != cl:
            err = err + 1
#            print "error at index %d: %d(%d)"%(d, lab, cl)
    return err / len(label)

"""
from sklearn.datasets import load_iris
from PCA import PCA
from LDA import LDA

data_set = load_iris()
data = data_set.data
label = data_set.target

new_data = LDA(data, label, 3)

sigma, mean = QDF(new_data, label, 3)
print (QDFTest(new_data, label, sigma, mean))

w,b = LDF(new_data, label, 3)
print (LDFTest(new_data, label, w, b))

print(NNTest(new_data, label, new_data, label))
"""
