import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris

from PCA import PCA
from LDA import LDA

def PlotData(data, label, clusters):
    color = ('red','green','blue')
    for cl in range(0, clusters):
        class_data = data[label == cl,:]
        plt.plot(class_data[:,0], class_data[:,1], '*', color = color[cl])

data_set = load_iris()
data = data_set.data
label = data_set.target

PCAData = PCA(data, 3)
LDAData = LDA(data, label, 3)


plt.figure()
PlotData(data, label, 3)
plt.title('Original data')
plt.xlabel('First dimension')
plt.ylabel('Second dimension')
plt.figure()
PlotData(PCAData, label, 3)
plt.title('PCAData')
plt.xlabel('First dimension')
plt.ylabel('Second dimension')
plt.figure()
PlotData(LDAData, label, 3)
plt.title('LDAData')
plt.xlabel('First dimension')
plt.ylabel('Second dimension')

plt.show()
