import numpy as np

"""
def zeroMean(dataMat):        
    meanVal=np.mean(dataMat,axis=0)       
    newData=dataMat-meanVal  
    return newData,meanVal  
  
def PCA(dataMat,n):  
    newData,meanVal=zeroMean(dataMat)  
    covMat=np.cov(newData,rowvar=0)    
      
    eigVals,eigVects=np.linalg.eig(np.mat(covMat))
    eigValIndice=np.argsort(eigVals)              
    n_eigValIndice=eigValIndice[-1:-(n+1):-1]     
    n_eigVect=eigVects[:,n_eigValIndice]          
    lowDDataMat=newData*n_eigVect                
    return lowDDataMat,n_eigVect
"""

def PCA(data, n):
    mean = np.mean(data, axis = 0)
    new_data = np.array(data - mean)
    cov_mat = np.cov(new_data, rowvar = 0)

    eig_vals, eig_vecs = np.linalg.eig(cov_mat)
    eig_idx = np.argsort(eig_vals)
    eig_idx = eig_idx[-1:-(n+1):-1]
    eig_vecs = eig_vecs[:,eig_idx]
    lowd_data = new_data.dot(eig_vecs)
    return lowd_data, eig_vecs
