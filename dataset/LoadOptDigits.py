import numpy as np
import os

def ListStrip(str_list):
    for i in range(0, len(str_list)):
        if str_list[i].strip() == '':
            str_list.pop(i)
    return str_list

def ParseLine(line):
    data = []
    label = 0
    data_label = line.split(',')
    data_label = ListStrip(data_label)

    for i in range(0, len(data_label)):
        if i == len(data_label) - 1:
            label = int(data_label[i])
        else:
            data.append(float(data_label[i]))

    return data, label

def LoadOptDigits():
    path = os.path.split(os.path.realpath(__file__))[0]
    train_file = open('%s/optdigits.tra'%path,'r')
    test_file = open('%s/optdigits.tes'%path,'r')

    train_data_label = train_file.read()
    test_data_label = test_file.read()

    train_file.close()
    test_file.close()

    train_data_label = ListStrip(train_data_label.split('\n'))
    test_data_label = ListStrip(test_data_label.split('\n'))

    train_data = []
    train_label = []

    for line in train_data_label:
        line_data, line_label = ParseLine(line)
        train_data.append(line_data)
        train_label.append(line_label)

    test_data = []
    test_label = []
    for line in test_data_label:
        line_data, line_label = ParseLine(line)
        test_data.append(line_data)
        test_label.append(line_label)

    train_data = np.array(train_data)
    test_data = np.array(test_data)
    train_label = np.array(train_label)
    test_label = np.array(test_label)

    return train_data, train_label, test_data, test_label
