#include <stdint.h>
#include <string.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <utility>
#include <map>
#include <vector>

#define LOG_INFO \
	std::cout << __FILE__ << ":[" << __LINE__ << "] "  

#define DISABLE_COPY_AND_ASSIGN(classname) \
	private: \
	  classname(const classname&); \
	  classname& operator=(const classname&)

namespace OLHWDB{

using std::string;
using std::pair;
using std::map;
using std::vector;
using std::istream;
using std::ostream;
using std::ifstream;
using std::ofstream;

typedef pair<int, vector<float> > Datum;

struct CodeMapRegistry{
  typedef map<int16_t, int> CodeMap;

  static void ReadCodeMapFrom(const char* file_name){

    CodeMap& registry = GetCodeMap();

    ifstream ifs(file_name);

    string line;

    while (std::getline(ifs, line)){
      if (line.size() == 0) continue;

      int pos = line.find_first_of(' ');
      if (pos == string::npos) continue;

      int16_t code = std::stoi(line.substr(0, pos));
      int label = std::stoi(line.substr(pos+1));

      registry[code] = label;
    }

    ifs.close();
  }
  static inline void ReadCodeMapFrom(const string& file_name){
    ReadCodeMapFrom(file_name.c_str());
  }

  static CodeMap& GetCodeMap(){
    static CodeMap* registry = new CodeMap();
    return *registry;
  }

  static int Get(int16_t code){
    CodeMap& registry = GetCodeMap();
    return registry[code];
  }
  

private:
  CodeMapRegistry(){}
  DISABLE_COPY_AND_ASSIGN(CodeMapRegistry);
};

struct Header {
  int32_t header_size; // 4B
  char format_code[8]; // 8B
  char* illustration; // header_size - 62
  char code_type[20]; // 20B
  int16_t code_length; // 2B
  char data_type[20];// 20B
  int32_t sample_number; // 4B
  int32_t dim; // 4B

  void ParseHeader(const char* header_buf);

  ~Header() {if(illustration) delete illustration;}

  private:
    bool initialized_{false};
    int last_header_size_{0};
    inline void ReAlloc(){
      if (!initialized_) 
        illustration = new char[header_size - 62];
      else if (last_header_size_ < header_size){
        delete illustration;
	illustration = new char[header_size - 62];
      }
      last_header_size_ = header_size;
    }
};

struct DBReader {
  DBReader():counter_(0){}
  DBReader(const char* file_name):counter_(0){
    Open(file_name);
  }
  DBReader(const string& file_name):counter_(0){
    Open(file_name);
  }

  void Open(const char* file_name);
  inline void Open(const string& file_name){
    Open(file_name.c_str());
  }

  inline void Close();

  inline bool Valid(){return counter_ < header_.sample_number;}

  inline int SampleSize(){return header_.dim + header_.code_length;}

  inline void SkipToFirst(){ifs_.seekg(header_.header_size); counter_ = 0;}

  void Read(Datum* datum);

  private:
    void FillHeader();
    Header header_;
    ifstream ifs_;
    int counter_;

    DISABLE_COPY_AND_ASSIGN(DBReader);
};

void Header::ParseHeader(const char* header_buf){
  int header_offset = 0;

  // Parse the header size (4 Bytes)
  memcpy((void*)(&header_size), header_buf + header_offset, 4);
  header_offset += 4;
//  LOG_INFO << "Header size: " << header_size << std::endl;

  // Parse the format code (8 Bytes)
  memcpy((void*)format_code, header_buf + header_offset, 8);
  header_offset += 8;
//  LOG_INFO << "Format code: " << format_code << std::endl;

  // Parse the illustration (header_size - 62 Bytes)
  ReAlloc();
  memcpy(illustration, header_buf + header_offset, header_size - 62);
  header_offset += (header_size - 62);
//  LOG_INFO << "Illustration: " << illustration << std::endl;

  // Parse the code type (20 Bytes)
  memcpy(code_type, header_buf + header_offset, 20);
  header_offset += 20;
//  LOG_INFO << "Code type: " << code_type << std::endl;

  // Parse the code length (2 Bytes)
  memcpy((void*)(&code_length), header_buf + header_offset, 2);
  header_offset += 2;
//  LOG_INFO << "Code length: " << code_length << std::endl;

  // Parse the data type (20 Bytes)
  memcpy(data_type, header_buf + header_offset, 20);
  header_offset += 20;
//  LOG_INFO << "Data type: " << data_type << std::endl;

  // Parse the sample number (4 Bytes)
  memcpy((void*)(&sample_number), header_buf + header_offset, 4);
  header_offset += 4;
//  LOG_INFO << "Sample number: " << sample_number << std::endl;

  // Parse the dimension (4 Bytes)
  memcpy((void*)(&dim), header_buf + header_offset, 4);
//  LOG_INFO << "Dimension: " << dim << std::endl;

  initialized_ = true;
}

void DBReader::Open(const char* file_name){
  if (ifs_.is_open()) ifs_.close();
  ifs_.open(file_name, std::ios::binary);
  FillHeader();
  counter_ = 0;
}

inline void DBReader::Close(){
  if (ifs_.is_open()) ifs_.close();
}

void DBReader::FillHeader(){
  int32_t header_size;
  ifs_.read((char*)(&header_size), 4);
  ifs_.seekg(0);

  char* buffer = new char[header_size];
  ifs_.read(buffer, header_size);

  header_.ParseHeader(buffer);

  delete buffer;
}

void DBReader::Read(Datum* datum){
  char* buffer = new char[SampleSize()];

  ifs_.read(buffer, SampleSize());

  // Parse label from buffer
  int16_t code;
  memcpy((void*)(&code), buffer, header_.code_length);
  datum->first = CodeMapRegistry::Get(code);

  // Parse data from buffer
  datum->second.clear();
  for (int d = 0; d < header_.dim; ++d){
    datum->second.push_back((float)*(unsigned char*)(buffer + header_.code_length + d));
  }

  counter_ ++;

  delete buffer;
}

ostream& operator<<(ostream& os, const Datum& datum){
  for (int d = 0; d < datum.second.size(); ++d){
    os << datum.second[d] << ' ';
  }

  os << datum.first << "\n";

  return os;
}

}

using namespace OLHWDB;
void WriteDBFromTo(const char* file_list, const char* out_name){
  std::ifstream ifs(file_list);
  std::ofstream ofs(out_name);
  std::string file_name;
  OLHWDB::DBReader db_reader;
  OLHWDB::Datum datum;

  while(std::getline(ifs, file_name)){
    LOG_INFO << "Reading from db file: " << file_name << std::endl;
    db_reader.Open(file_name);
    while (db_reader.Valid()){
      db_reader.Read(&datum);
      ofs << datum;
    }
  }

  db_reader.Close();
  ifs.close();
  ofs.close();
}

int main(){
  OLHWDB::CodeMapRegistry::ReadCodeMapFrom("label_map.txt");

  LOG_INFO << "Parsing from file list: label_map.txt\n";
  WriteDBFromTo("OLHWDB1.1trn/list.txt", "OLHWDB1.1.trn");
  WriteDBFromTo("OLHWDB1.1tst/list.txt", "OLHWDB1.1.tst");

  return 0;

}
