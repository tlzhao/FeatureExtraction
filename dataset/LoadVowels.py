import numpy as np
import os

def ListStrip(line):
    for i in range(0, len(line)):
        if line[i].strip() == '':
            line.pop(i)
    return line

def ParseLine(line):
    data_str = line.split(' ')
    data_str = ListStrip(data_str)
    data = []
    for d in data_str:
        data.append(float(d))
    return data

def ParseBlock(block, cluster):
    block = block.split('\n')
    blobk = ListStrip(block)

    data = []
    label = []

    for line in blobk:
        data.append(ParseLine(line))
        label.append(cluster)
    return data, label

def LoadVowels():
    path = os.path.split(os.path.realpath(__file__))[0]
    train_file = open('%s/ae.train'%path,'r')
    test_file = open('%s/ae.test'%path,'r')

    train_data_label = ListStrip(train_file.read().split('\n\n'))
    test_data_label = ListStrip(test_file.read().split('\n\n'))

    train_file.close()
    test_file.close()

    train_data = []
    train_label = []
    train_num = [30, 30, 30, 30, 30, 30, 30, 30, 30];
    n = 0;
    for l in range(0,9):
        for idx in range(0, train_num[l]):
            data, label = ParseBlock(train_data_label[n], l)
            train_data = train_data + data
            train_label = train_label + label
            n = n + 1
    train_data = np.array(train_data)
    train_label = np.array(train_label)

    test_data = []
    test_label = []
    test_num = [31, 35, 88, 44, 29, 24, 40, 50, 29]
    n = 0
    for l in range(0, 9):
        for idx in range(0, test_num[l]):
            data, label = ParseBlock(test_data_label[n], l)
            test_data = test_data + data
            test_label = test_label + label
            n = n + 1
    test_data = np.array(test_data)
    test_label = np.array(test_label)

    return train_data, train_label, test_data, test_label

