from __future__ import division
import numpy as np
import os

def ListStrip(line, char = ''):
    for i in range(0, len(line)):
        if line[i].strip() == char:
            line.pop(i)
    return line

def ParseLine(line):
    data = []
    label = 0
    data_label = line.split(' ')
    for i in range(0, len(data_label)):
        if i == len(data_label) - 1:
            label = int(data_label[i])
        else:
            data.append(float(data_label[i]))
    return data, label

def LoadOLHWDB():
    path = os.path.split(os.path.realpath(__file__))[0]
    train_file = open('%s/OLHWDB1.1.trn'%path, 'r')
    test_file = open('%s/OLHWDB1.1.tst'%path, 'r')

    train_data = []
    train_label = []
    count = 0
    while 1:
        line = train_file.readline()
        if len(line) == 0:
            break;
        else:
            line_data, line_label = ParseLine(line)
            train_data.append(line_data)
            train_label.append(line_label)
            count = count + 1
            print ("%d training data read"%count)

    test_data = []
    test_label = []
    count = count + 1
    while 1:
        line = test_file.readline()
        if len(line) == 0:
            break;
        else:
            line_data, line_label = ParseLine(line)
            test_data.append(line_data)
            test_label.append(line_label)
            count = count + 1
            print ("%d test data read"%count)

    train_data = np.array(train_data)
    test_data = np.array(test_data)
    train_label = np.array(train_label)
    test_label = np.array(test_label)

    train_file.close()
    test_file.close()

    return train_data, train_label, test_data, test_label

#train_data, train_label, test_data, test_label = LoadOLHWDB();
#np.save("OLHWDB.npy", [train_data, train_label, test_data, test_label]);
