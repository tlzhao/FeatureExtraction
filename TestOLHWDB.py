from dataset.LoadOLHWDB import LoadOLHWDB
from dataset.LoadSat import LoadSat
import numpy as np
import LDA
from Classifiers import ProtoType, ProtoTypeTest, LDF, LDFTest

train_data, train_label, test_data, test_label = LoadOLHWDB()

data = np.concatenate((train_data, test_data), axis = 0)

out_file = open('output.txt', 'w')

mean = data.mean(axis = 0)
data = data - mean
var = np.cov(data, rowvar = 0)
eig_vals, eig_vecs = np.linalg.eig(var)
eig_idx = np.argsort(eig_vals)
eig_idx = eig_idx[-1:-101:-1]
eig_vecs = eig_vecs[:,eig_idx]

train_data_pca = train_data.dot(eig_vecs)
test_data_pca = test_data.dot(eig_vecs)

print ("Results on PCA-LDF")
for k  in range(1,11):
    pca_train_data = train_data_pca[:, 0:(10*k)]
    pca_test_data = test_data_pca[:,0:(10*k)]
    ldf_w, ldf_b = LDF(pca_train_data, train_label, 3755)
    print ("k=%d"%k)
    print ("%f"%(1-LDFTest(pca_test_data, test_label, ldf_w, ldf_b)))


print ("Results on PCA-NPK")
for k in range(1, 11):
    pca_train_data = train_data_pca[:,0:(10*k)]
    pca_test_data = test_data_pca[:,0:(10*k)]
    for num_proto in range(1, 4):
        proto, plabel = ProtoType(pca_train_data, train_label, 3755, num_proto)
        print ("K = %d, N = %d"%(k, num_proto))
        print ("%f"%(1-ProtoTypeTest(pca_test_data, test_label, proto, plabel)))
        out_file.write(str.format("K = %d, N = %d:\n",(k, num_proto)))
        out_file.write(str.format("%f\n"%(1 - ProtoTypeTest(pca_test_data, test_label, proto, plabel))))

SW = LDA.WithinClassSW(train_data, train_label, 3755)
ST = LDA.TotalClassST(train_data)
SB = ST - SW
eig_vals, eig_vecs = np.linalg.eig(np.linalg.pinv(SW).dot(SB))
eig_idx = np.argsort(eig_vals)
eig_idx = eig_idx[-1:-101:-1]
eig_vecs = eig_vecs[:,eig_idx]

train_data_lda = train_data.dot(eig_vecs)
test_data_lda = test_data.dot(eig_vecs)

print ("Results on LDA-LDF")
for k in range(1,11):
    lda_train_data = train_data_lda[:,0:(10*k)]
    lda_test_data = test_data_lda[:,0:(10*k)]
    ldf_w, ldf_b = LDF(lda_train_data, train_label, 3755)
    print ("k = %d"%k)
    print ("%f"%(1-LDFTest(lda_test_data, test_label, ldf_w, ldf_b)))

print ("Results on LDA-NPK")
for k in range(1:11):
    lda_train_data = train_data_lda[:,0:(10*k)]
    lda_test_data = test_data_lda[:,0:(10*k)]
    for num_proto in range(1,4):
        proto, plabel = ProtoType(lda_train_data, train_label, 3755)
        print ("k = %d, n = %d"%(k, num_proto))
        print ("%f"%(1 - ProtoTypeTest(lda_test_data, test_label, proto, plabel)))
out_file.close()
