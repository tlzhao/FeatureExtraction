import numpy as np

def ClassMean(data, label, clusters):
    mean_vector = [];
    for cl in range(0, clusters):
        mean_vector.append(np.mean(data[label == cl,:], axis = 0));
    return mean_vector

def WithinClassSW(data, label, clusters):
    m = data.shape[1]
    sw = np.zeros([m,m]);
    mean_vector = ClassMean(data, label, clusters)
    for cl, mv in zip(range(0, clusters), mean_vector):
        class_data = data[label == cl,:] - mv
        sw += np.cov(class_data, rowvar = 0)
    return sw


def TotalClassST(data):
    mean_vector = np.mean(data, axis = 0)
    new_data = data - mean_vector
    st = np.cov(new_data, rowvar = 0)
    return st

def LDA(data, label, clusters):
    SW = WithinClassSW(data, label, clusters)
    ST = TotalClassST(data)
    SB = ST - SW
    eig_vals, eig_vecs = np.linalg.eig(np.linalg.pinv(SW).dot(SB))
    eig_idx = np.argsort(eig_vals)
    eig_idx = eig_idx[-1:-clusters:-1]
    eig_vecs = eig_vecs[:,eig_idx]
    lda_data = data.dot(eig_vecs)
    return lda_data, eig_vecs
